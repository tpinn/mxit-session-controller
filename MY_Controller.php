<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MY_Controller Class
 *
 * Manage sessions for the Mxit environment. It is adapted from CodeIgniter's
 * Session library.
 *
 * @author Terry Ross Pinn
 * @source https://bitbucket.org/tpinn/mxit-session-controller
 */
class MY_Controller extends CI_Controller {

	/**
	 * Unique identifier of mxit user.
	 *
	 * @var string
	 * @access protected
	 */
	protected $mxit_id = '';

	/**
	 * The user’s current nickname.
	 *
	 * @var string
	 * @access protected
	 */
	protected $mxit_nick = '';


	/**
	 * The mxit app name.
	 *
	 * @var string
	 * @access protected
	 */
	protected $mxit_app_name = '';


	/**
	 * Unique identifier within the session array.
	 *
	 * @var string
	 * @access private
	 */
	private $session_id = '';


	/**
	 * This url will be used to redirect a user, if the person is not a mxit user.
	 * Change this to the url you desire, for example: http://yourblog.com
	 *
	 * @var string
	 * @access private
	 */
	private $redirect_url = 'http://yourblog.com';

	/**
	 * Constructor
	 *
	 * The constructor will check if the person is a mxit user, load information
	 * and check the session. Please read the comments of each method to gain a
	 * better understanding of what each one does.
	 *
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();

		$this->initialise();
		$this->is_mxit_user();
		$this->check_session();
	}

	/**
	 * Load mxit information and create unique identifier within the session array.
	 *
	 * @access	private
	 * @return	void
	 */
	private function initialise()
	{
		$this->mxit_id = (string) isset($_SERVER['HTTP_X_MXIT_USERID_R']) ? $_SERVER['HTTP_X_MXIT_USERID_R'] : '';
		$this->mxit_nick = (string) isset($_SERVER['HTTP_X_MXIT_NICK']) ? $_SERVER['HTTP_X_MXIT_NICK'] : '';
		$this->mxit_app_name = (string) isset($_SERVER['HTTP_X_MXIT_APP_NAME']) ? $_SERVER['HTTP_X_MXIT_APP_NAME'] : '';

		// This is work around to php bug - Unknown: Skipping numeric key 1 in Unknown on line 0
		$this->session_id = 'm_' . $this->mxit_id;
	}

	/**
	 * Verify the site is only accessed by a mxit user. If not, the user will be
	 * redirected to the url you specify.
	 *
	 * @access	private
	 * @return	void
	 */
	private function is_mxit_user()
	{
		if (empty($this->mxit_id))
		{
			redirect($this->redirect_url);
		}
	}

	/**
	 * Create a new session with mxit id prepended with 'm_' as the key. Or it will
	 * resume the current session.
	 *
	 * This method will also wipe the session of previous data if users manage to get
	 * other's sessions.
	 *
	 * Redirects user to site controller if they do not have a session.
	 *
	 * @access	private
	 * @return	void
	 */
	private function check_session()
	{
		session_start();

		if ( ! isset($_SESSION[$this->session_id]))
		{
			$_SESSION = array();
			$_SESSION[$this->session_id] = array();

			if ($this->router->fetch_class() !== 'site')
			{
				redirect();
			}
		}
	}

	/**
	 * Add or change data in the session array
	 *
	 * @access	public
	 * @param	mixed	array or string
	 * @param	string
	 * @return	void
	 */
	public function set_userdata($newdata = array(), $newval = '')
	{
		if (is_string($newdata))
		{
			$newdata = array($newdata => $newval);
		}

		if (count($newdata) > 0)
		{
			foreach ($newdata as $key => $val)
			{
				$_SESSION[$this->session_id][$key] = $val;
			}
		}
	}

	/**
	 * Delete a session variable from the session array
	 *
	 * @access	public
	 * @param	mixed	array or string
	 * @return	void
	 */
	public function unset_userdata($data)
	{
		if (is_string($data))
		{
			$data = array($data);
		}

		if (count($data) > 0)
		{
			foreach ($data as $val)
			{
				unset($_SESSION[$this->session_id][$val]);
			}
		}
	}

	/**
	 * Kill the session altogether
	 *
	 * @access	public
	 * @return	void
	 */
	public function destroy_session()
	{
		$_SESSION = array();

		// Delete the session cookie. This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies"))
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		session_destroy();
	}

	/**
	 * Fetch a specific item from the session array
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function userdata($item)
	{
		return ( ! isset($_SESSION[$this->session_id][$item])) ? FALSE : $_SESSION[$this->session_id][$item];
	}

	/**
	 * Fetch all session data
	 *
	 * @access	public
	 * @return	array
	 */
	public function all_userdata()
	{
		return $_SESSION[$this->session_id];
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */